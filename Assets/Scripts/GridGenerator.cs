using System.Collections.Generic;
using DefaultNamespace;
using Unity.VisualScripting;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{

    #region GridTiles
    private const int TILE_NUMBER_X = 30;
    private const int TILE_NUMBER_Y = 30;
    [field:SerializeField] private Vector2 StartingPoint { get; set; }
    [field:SerializeField] public float BreakBetweenTiles { get; private set; }
    [field:SerializeField] public float TileSize { get; private set; }
    [SerializeField] private GameObject _tilePrefab;
    [SerializeField] private GameObject _gridParent;
    
    #endregion

    #region PlayerSpawn
    [field:SerializeField] public Vector2 PlayerSpawnCoordinates { get; set; }
    [SerializeField] private GameObject _playerPrefab;

    #endregion

    #region EnemySpawn
    [field:SerializeField] public Vector2 EnemySpawnCoordinates { get; set; }
    [SerializeField] private GameObject _enemyPrefab;

    #endregion
    
    #region ExitPointSpawn
    [field:SerializeField] public Vector2 ExitPointSpawnCoordinates { get; set; }
    [SerializeField] private GameObject _exitPointPrefab;

    #endregion
    
    #region WallSpawn
    [field:SerializeField] public List<Wall> Walls { get; set; }
    [SerializeField] private GameObject _wallPrefab;
    [SerializeField] private GameObject _wallParent;

    #endregion
    

    void Awake()
    {
        ValidateLevelConfiguration();
        InstantiateTiles();
        InstantiateTheseus();
        InstantiateMinotaur();
        InstantiateExitPoint();
        InstantiateWalls();
    }
    
    private void ValidateLevelConfiguration()
    {
        if (PlayerSpawnCoordinates.x == EnemySpawnCoordinates.x && PlayerSpawnCoordinates.y == EnemySpawnCoordinates.y)
        {
            Debug.LogError("ERROR: PLAYER AND ENEMY CAN NOT START AT THE SAME COORDINATE!");
        }
        
        if (PlayerSpawnCoordinates.x >= TILE_NUMBER_X || PlayerSpawnCoordinates.y >= TILE_NUMBER_Y)
        {
            Debug.LogError("ERROR: PLAYER CAN NOT START OUTSIDE THE GRID!");
        }
        
        if (EnemySpawnCoordinates.x >= TILE_NUMBER_X || EnemySpawnCoordinates.y >= TILE_NUMBER_Y)
        {
            Debug.LogError("ERROR: ENEMY CAN NOT START OUTSIDE THE GRID!");
        }
    }

    private void InstantiateTiles()
    {
        Vector2 spawnPoint;
        for (int x = 0; x < TILE_NUMBER_X; x++)
        {
            for (int y = 0; y < TILE_NUMBER_Y; y++)
            {
                spawnPoint = new Vector2(StartingPoint.x + x * (TileSize + BreakBetweenTiles),
                    StartingPoint.y + y * (TileSize + BreakBetweenTiles));
                GameObject tileGMO = Instantiate(_tilePrefab, _gridParent.transform);

                tileGMO.transform.position = spawnPoint;
            }
        }
    }

    private void InstantiateTheseus()
    {
        Vector2 spawnPoint = new Vector2(StartingPoint.x + PlayerSpawnCoordinates.x * (TileSize + BreakBetweenTiles),
            StartingPoint.y + PlayerSpawnCoordinates.y * (TileSize + BreakBetweenTiles));
        
        GameObject playerGMO = Instantiate(_playerPrefab);

        playerGMO.transform.position = spawnPoint;
    }

    private void InstantiateMinotaur()
    {
        Vector2 spawnPoint = new Vector2(StartingPoint.x + EnemySpawnCoordinates.x * (TileSize + BreakBetweenTiles),
            StartingPoint.y + EnemySpawnCoordinates.y * (TileSize + BreakBetweenTiles));
        
        GameObject enemyGMO = Instantiate(_enemyPrefab);

        enemyGMO.transform.position = spawnPoint;
    }
    
    private void InstantiateExitPoint()
    {
        Vector2 spawnPoint = new Vector2(StartingPoint.x + ExitPointSpawnCoordinates.x * (TileSize + BreakBetweenTiles),
            StartingPoint.y + ExitPointSpawnCoordinates.y * (TileSize + BreakBetweenTiles));
        
        GameObject exitPointGMO = Instantiate(_exitPointPrefab);

        exitPointGMO.transform.position = spawnPoint;
    }

    private void InstantiateWalls()
    {
        Vector2 spawnPoint;

        foreach (Wall wall in Walls)
        {
            int remainingTileLength = wall.TileLength;
            int builtTileNumber = 0;
            while (builtTileNumber < remainingTileLength)
            {
                GameObject wallGMO = Instantiate(_wallPrefab, _wallParent.transform);

                if (wall.Direction == WallDirection.HORIZONTAL)
                {
                    spawnPoint = new Vector2(StartingPoint.x + (wall.StartingCoordinate.x + builtTileNumber) * (TileSize + BreakBetweenTiles),
                        StartingPoint.y + wall.StartingCoordinate.y * (TileSize + BreakBetweenTiles) - TileSize/2);
                }
                else
                {
                    spawnPoint = new Vector2(StartingPoint.x + wall.StartingCoordinate.x * (TileSize + BreakBetweenTiles) - TileSize/2,
                        StartingPoint.y + (wall.StartingCoordinate.y + builtTileNumber) * (TileSize + BreakBetweenTiles));
                    
                    wallGMO.transform.Rotate(0, 0, 90);
                }

                wallGMO.transform.position = spawnPoint;
                
                builtTileNumber++;
            }
        }
    }
    
}
