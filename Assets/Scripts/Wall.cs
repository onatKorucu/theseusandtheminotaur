using UnityEngine;

namespace DefaultNamespace
{

    [CreateAssetMenu(menuName = "Custom/Wall", order = 0)]
    public class Wall : ScriptableObject
    {
        [field:SerializeField] public Vector2 StartingCoordinate { get; set; }
        [field:SerializeField] public WallDirection Direction { get; set; }
        [field:SerializeField] public int TileLength { get; private set; }
    }
}