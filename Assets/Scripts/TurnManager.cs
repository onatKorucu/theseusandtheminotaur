using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class TurnManager : MonoBehaviour
    {
        private static bool isPlayersTurn = true;

        public static event Action<bool> OnTurnChanged;

        private void Awake()
        {
            isPlayersTurn = true;
        }

        public static void ChangeTurn()
        {
            isPlayersTurn = !isPlayersTurn;

            OnTurnChanged(isPlayersTurn);
        }

        public static bool GetIsPlayersTurn()
        {
            return isPlayersTurn;
        }

    }
}