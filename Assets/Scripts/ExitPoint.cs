using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
    public class ExitPoint : MonoBehaviour
    {
        [SerializeField, ReadOnly] protected GridGenerator _gridGenerator;
        [SerializeField, ReadOnly] protected PlayerMovement _playerMovement;

        [SerializeField, ReadOnly] public Vector2 coordinates;

        protected void Awake()
        {
            TurnManager.OnTurnChanged += HandleOnTurnChanged;
            
            if (_gridGenerator == null)
            {
                _gridGenerator = FindObjectOfType<GridGenerator>();
            }            
            
            if (_playerMovement == null)
            {
                _playerMovement = FindObjectOfType<PlayerMovement>();
            }
            
            InitializeCoordinates();
        }

        private void OnDestroy()
        {
            TurnManager.OnTurnChanged -= HandleOnTurnChanged;
        }
        
        private void InitializeCoordinates()
        {
            coordinates = _gridGenerator.ExitPointSpawnCoordinates;
        }

        private void HandleOnTurnChanged(bool obj)
        {
            if (PlayerReachedExit())
            {
                CustomSceneManager.instance.LoadNextLevel();
            }
        }

        private bool PlayerReachedExit()
        {
            if (coordinates.x == _playerMovement.coordinates.x && coordinates.y == _playerMovement.coordinates.y)
            {
                return true;
            }

            return false;
        }
    }
}