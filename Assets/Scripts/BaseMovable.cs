using Unity.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public abstract class BaseMovable : MonoBehaviour
    {
        [SerializeField, ReadOnly] public Vector2 coordinates;

        [SerializeField, ReadOnly] protected GridGenerator _gridGenerator;

        private protected BaseMovable _movableUnit;
        
        public float moveTime = 1;
        public bool isMoving;

        private Vector3 startPosition;
        private Vector3 targetPosition;

        protected virtual void Awake()
        {
            if (_gridGenerator == null)
            {
                _gridGenerator = FindObjectOfType<GridGenerator>();
            }
            
            moveTime = 1;
        }

        protected virtual void Update()
        {
            MoveSmoothlyIfNeeded();
        }

        private void MoveSmoothlyIfNeeded()
        {
            if (moveTime < 1)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, moveTime);
                moveTime += Time.deltaTime;
            }
            else
            {
                if (isMoving)
                {
                    transform.position = targetPosition;
                }
                isMoving = false;
                moveTime = 1;
            }
        }

        protected virtual void Move(Vector2 direction)
        {
            ChangeCoordinates(direction);
        
            Vector2 pushVector = direction * (_gridGenerator.TileSize + _gridGenerator.BreakBetweenTiles);
            
            startPosition = transform.position;
            
            targetPosition = transform.position + (Vector3)pushVector;
            
            moveTime = 0;

            isMoving = true;
        }

        private void ChangeCoordinates(Vector2 direction)
        {
            coordinates += direction;
        }
        
        protected bool IsWallBlockingDirection(Vector2 movementDirection)
        {
            foreach (Wall wall in _gridGenerator.Walls)
            {
                if (movementDirection == Vector2.down)
                {
                    //Check horizontal walls
                    if (wall.Direction == WallDirection.VERTICAL) { continue; }

                    if (_movableUnit.coordinates.y == wall.StartingCoordinate.y &&
                        wall.StartingCoordinate.x <= _movableUnit.coordinates.x &&
                        wall.StartingCoordinate.x + wall.TileLength > _movableUnit.coordinates.x)
                    {
                        return true;
                    }
                    
                }
                else if (movementDirection == Vector2.up)
                {
                    //Check horizontal walls
                    if (wall.Direction == WallDirection.VERTICAL) { continue; }

                    if (_movableUnit.coordinates.y + 1 == wall.StartingCoordinate.y &&
                        wall.StartingCoordinate.x <= _movableUnit.coordinates.x &&
                        wall.StartingCoordinate.x + wall.TileLength > _movableUnit.coordinates.x)
                    {
                        return true;
                    }
                }
                else if (movementDirection == Vector2.left)
                {
                    //Check vertical walls
                    if (wall.Direction == WallDirection.HORIZONTAL) { continue; }

                    if (_movableUnit.coordinates.x == wall.StartingCoordinate.x &&
                        wall.StartingCoordinate.y <= _movableUnit.coordinates.y &&
                        wall.StartingCoordinate.y + wall.TileLength > _movableUnit.coordinates.y)
                    {
                        return true;
                    }
                }
                else if (movementDirection == Vector2.right)
                {
                    //Check vertical walls
                    if (wall.Direction == WallDirection.HORIZONTAL) { continue; }

                    if (_movableUnit.coordinates.x + 1 == wall.StartingCoordinate.x &&
                        wall.StartingCoordinate.y <= _movableUnit.coordinates.y &&
                        wall.StartingCoordinate.y + wall.TileLength > _movableUnit.coordinates.y)
                    {
                        return true;
                    }
                }

            }
            return false;
        }
        
        protected virtual void SetMovableUnit()
        {
            
        }
    }
}