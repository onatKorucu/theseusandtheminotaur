using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
    public class CustomSceneManager : MonoBehaviour
    {
        private const int LEVEL_COUNT = 3;

        public event Action OnGameCompleted;

        public static CustomSceneManager instance;

        void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                RestartCurrentLevel();
            }else
            if (Input.GetKeyDown(KeyCode.N))
            {
                LoadNextLevel();
            }else if (Input.GetKeyDown(KeyCode.P))
            {
                LoadPreviousLevel();
            }
        }

        public void RestartCurrentLevel()
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentSceneIndex);
        }

        public void LoadNextLevel()
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

            if (currentSceneIndex + 1 >= LEVEL_COUNT)
            {
                OnGameCompleted();
                return;
            }
            
            SceneManager.LoadScene(currentSceneIndex + 1);
        }

        public void LoadPreviousLevel()
        {
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            
            if (currentSceneIndex - 1 < 0) { return; }
            
            SceneManager.LoadScene(currentSceneIndex - 1);
        }
        
    }
}