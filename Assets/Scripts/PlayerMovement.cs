using System.Collections;
using DefaultNamespace;
using UnityEngine;

public class PlayerMovement : BaseMovable
{
    protected override void Awake()
    {
        base.Awake();
        SetMovableUnit();
        InitializeCoordinates();
    }
    
    protected override void Update()
    {
        base.Update();

        if (!TurnManager.GetIsPlayersTurn()) { return; }
        
        if (isMoving) { return; }
        
        if (Input.GetKeyDown(KeyCode.W)) //Make Theseus wait and pass the turn
        {
            TurnManager.ChangeTurn();
        }else
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            StartCoroutine(MoveAndGiveTurn(Vector2.up));
        }else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            StartCoroutine(MoveAndGiveTurn(Vector2.down));
        }else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StartCoroutine(MoveAndGiveTurn(Vector2.left));
        }else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StartCoroutine(MoveAndGiveTurn(Vector2.right));
        }
    }

    protected override void SetMovableUnit()
    {
        base.SetMovableUnit();
        _movableUnit = FindObjectOfType<PlayerMovement>();
    }
    
    private void InitializeCoordinates()
    {
        coordinates = _gridGenerator.PlayerSpawnCoordinates;
    }

    private IEnumerator MoveAndGiveTurn(Vector2 direction)
    {
        if (IsWallBlockingDirection(direction)) { yield break; }

        base.Move(direction);
        yield return new WaitForSeconds(1f);
        TurnManager.ChangeTurn();
    }

}
