using DefaultNamespace;
using TMPro;
using UnityEngine;

public class YouWonTextController : MonoBehaviour
{
    [SerializeField] private GameObject _youWonGMO;

    void Awake()
    {
        CustomSceneManager.instance.OnGameCompleted += HandleOnGameCompleted;
    }

    void OnDestroy()
    {
        CustomSceneManager.instance.OnGameCompleted -= HandleOnGameCompleted;
    }

    private void HandleOnGameCompleted()
    {
        EnableYouWonText();
    }

    private void EnableYouWonText()
    {
        _youWonGMO.SetActive(true);
    }
}
