using System.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public class EnemyMovement : BaseMovable
    {
        Vector2 direction;
        private PlayerMovement _playerMovement;
        
        protected override void Awake()
        {
            base.Awake();
            TurnManager.OnTurnChanged += HandleOnTurnChanged;
            _playerMovement = FindObjectOfType<PlayerMovement>();
            SetMovableUnit();
            InitializeCoordinates();
        }

        private void OnDestroy()
        {
            TurnManager.OnTurnChanged -= HandleOnTurnChanged;
        }

        private void HandleOnTurnChanged(bool isPlayersTurn)
        {
            if (isPlayersTurn) { return; }

            StartCoroutine(MoveTwiceAtMost());
        }

        private void CheckIfEnemyCaughtPlayer()
        {
            if (HasEnemyCaughtPlayer())
            {
                CustomSceneManager.instance.RestartCurrentLevel();
            }
        }

        private IEnumerator MoveTwiceAtMost()
        {
            bool willMoveOnce = MoveIfBeneficial();
            if (willMoveOnce)
            {
                yield return new WaitForSeconds(1f);
            }
            
            CheckIfEnemyCaughtPlayer();

            bool willMoveTwice = MoveIfBeneficial();
            if (willMoveTwice)
            {
                yield return new WaitForSeconds(1f);
            }
            
            CheckIfEnemyCaughtPlayer();

            TurnManager.ChangeTurn();
        }

        private bool MoveIfBeneficial()
        {
            bool willMove = CanGetCloserToPlayer(out direction);
            if (willMove)
            {
                Move(direction);
            }
            
            return willMove;
        }
        
        private bool HasEnemyCaughtPlayer()
        {
            if (coordinates.x == _playerMovement.coordinates.x && coordinates.y == _playerMovement.coordinates.y)
            {
                return true;
            }

            return false;
        }

        private bool CanGetCloserToPlayer(out Vector2 direction)
        {
            direction = Vector2.zero;
            
            float currentDistance = Vector2.Distance(_playerMovement.coordinates,coordinates);

            if (currentDistance < 0.1f) { return false; }

            if (Vector2.Distance(_playerMovement.coordinates,coordinates + Vector2.left) < currentDistance && !IsWallBlockingDirection(Vector2.left))
            {
                direction = Vector2.left;
            }else if (Vector2.Distance(_playerMovement.coordinates,coordinates + Vector2.right) < currentDistance && !IsWallBlockingDirection(Vector2.right))
            {
                direction = Vector2.right;
            }else if (Vector2.Distance(_playerMovement.coordinates,coordinates + Vector2.down) < currentDistance && !IsWallBlockingDirection(Vector2.down))
            {
                direction = Vector2.down;
            }else if (Vector2.Distance(_playerMovement.coordinates,coordinates + Vector2.up) < currentDistance && !IsWallBlockingDirection(Vector2.up))
            {
                direction = Vector2.up;
            } 

            if (direction == Vector2.zero) { return false; }

            return true;
        }

        protected override void SetMovableUnit()
        {
            base.SetMovableUnit();
            _movableUnit = FindObjectOfType<EnemyMovement>();
        }
        
        private void InitializeCoordinates()
        {
            coordinates = _gridGenerator.EnemySpawnCoordinates;
        }

    }
}